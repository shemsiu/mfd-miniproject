import {Component} from '@angular/core';


import {HeaderComponent} from './header.component';
import {FooterComponent} from "./footer.component";
import {DemoProductService} from "./services/demo-products.service";

@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html',
    directives: [HeaderComponent, FooterComponent],
    providers: [DemoProductService]
})
export class AppComponent {
    title: string = "Indhold";

    goBack() {
        window.history.back();
    }
}