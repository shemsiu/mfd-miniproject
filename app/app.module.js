"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var app_routing_1 = require('./app.routing');
var app_component_1 = require('./app.component');
var header_component_1 = require('./header.component');
var footer_component_1 = require("./footer.component");
var featured_component_1 = require("./featured.component");
var singleproduct_component_1 = require("./singleproduct.component");
var filter_component_1 = require("./filter.component");
var frontpage_component_1 = require("./frontpage.component");
var headerslider_component_1 = require("./headerslider.component");
var productdetail_component_1 = require("./productdetail.component");
var specialproduct_component_1 = require("./specialproduct.component");
var contact_component_1 = require("./contact.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, app_routing_1.routing],
            declarations: [
                app_component_1.AppComponent,
                header_component_1.HeaderComponent,
                contact_component_1.ContactComponent,
                footer_component_1.FooterComponent,
                singleproduct_component_1.SingleProductComponent,
                filter_component_1.FilterComponent,
                frontpage_component_1.FrontpageComponent,
                headerslider_component_1.HeaderSliderComponent,
                productdetail_component_1.ProductDetailComponent,
                specialproduct_component_1.SpecialProductsComponent,
                featured_component_1.FeaturedComponent
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map