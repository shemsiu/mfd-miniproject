import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {routing} from './app.routing';

import {AppComponent}  from './app.component';

import {HeaderComponent} from './header.component';
import {FooterComponent} from "./footer.component";
import {FeaturedComponent} from "./featured.component";
import {SingleProductComponent} from "./singleproduct.component";
import {FilterComponent} from "./filter.component";
import {FrontpageComponent} from "./frontpage.component";
import {HeaderSliderComponent} from "./headerslider.component";
import {ProductDetailComponent} from "./productdetail.component";
import {SpecialProductsComponent} from "./specialproduct.component";
import {ContactComponent} from "./contact.component";

@NgModule({
    imports: [BrowserModule, FormsModule, routing],
    declarations: [
        AppComponent,
        HeaderComponent,
        ContactComponent,
        FooterComponent,
        SingleProductComponent,
        FilterComponent,
        FrontpageComponent,
        HeaderSliderComponent,
        ProductDetailComponent,
        SpecialProductsComponent,
        FeaturedComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
