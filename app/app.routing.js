"use strict";
var router_1 = require('@angular/router');
var frontpage_component_1 = require("./frontpage.component");
var singleproduct_component_1 = require("./singleproduct.component");
var contact_component_1 = require("./contact.component");
exports.routes = [
    { path: '', component: frontpage_component_1.FrontpageComponent },
    { path: 'product/:productId', component: singleproduct_component_1.SingleProductComponent },
    { path: 'contact', component: contact_component_1.ContactComponent }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routing.js.map