import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {FrontpageComponent} from "./frontpage.component";
import {SingleProductComponent} from "./singleproduct.component";
import {ContactComponent} from "./contact.component";

export const routes: Routes = [
    {path: '', component:FrontpageComponent},
    {path: 'product/:productId', component:SingleProductComponent},
    {path: 'contact', component:ContactComponent}
];

export const routing:ModuleWithProviders = RouterModule.forRoot(routes);
