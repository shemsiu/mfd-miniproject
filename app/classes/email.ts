export class Email {
    fullname: string;
    email: string;
    message: string;
}