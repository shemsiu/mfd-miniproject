"use strict";
var Product = (function () {
    function Product(id, title, subtitle, logo, images, price, discount, description, shipping, sizes, maxqty, brand, tabs) {
        if (sizes === void 0) { sizes = ["small", "medium", "large"]; }
        if (maxqty === void 0) { maxqty = 5; }
        if (brand === void 0) { brand = 0 /* adidas */; }
        if (tabs === void 0) { tabs = []; }
        this.discount = 0;
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.logo = logo;
        this.images = images;
        this.price = price;
        this.discount = discount;
        this.description = description;
        this.shipping = shipping;
        this.sizes = sizes;
        this.instock = maxqty;
        this.brand = brand;
        this.tabs = tabs;
    }
    //
    Product.prototype.calcDiscount = function () {
        return (this.discount / this.price) * 100;
    };
    //Calculates the price with discount
    Product.prototype.getPrice = function () {
        return this.price - this.discount;
    };
    Product.prototype.getIntroImage = function () {
        if (this.images.length > 0) {
            return this.images[0];
        }
        return "empty.png";
    };
    Product.prototype.minusStock = function (value) {
        this.instock = this.instock - value;
    };
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map