import {Brand} from "./brand";
import {Tabs} from "./tabs";

export class Product {
    id: number;
    title: string;
    subtitle: string;
    logo: string;
    images: string[];

    price: number;
    discount: number = 0;
    description: string;
    shipping: number;
    sizes: string[];
    instock: number;

    brand: Brand;

    tabs:Tabs[];

    constructor(id: number,
                title: string,
                subtitle: string,
                logo: string,
                images: string[],
                price: number,
                discount: number,
                description: string,
                shipping: number,
                sizes: string[] = ["small", "medium", "large"],
                maxqty: number = 5,
                brand: Brand = Brand.adidas,
                tabs:Tabs[] = []
    ) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.logo = logo;
        this.images = images;
        this.price = price;
        this.discount = discount;
        this.description = description;
        this.shipping = shipping;
        this.sizes = sizes;
        this.instock = maxqty;
        this.brand = brand;
        this.tabs = tabs;
    }

    //
    calcDiscount() {
        return (this.discount / this.price) * 100;
    }

    //Calculates the price with discount
    getPrice() {
        return this.price - this.discount;
    }

    getIntroImage() {
        if (this.images.length > 0) {
            return this.images[0];
        }

        return "empty.png";
    }

    minusStock(value:number) {
        this.instock = this.instock - value;
    }
}