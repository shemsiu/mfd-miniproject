"use strict";
var Tabs = (function () {
    function Tabs(title, slug, content) {
        this.title = title;
        this.slug = slug;
        this.content = content;
        this.hide();
    }
    Tabs.prototype.show = function () {
        this.display = true;
    };
    Tabs.prototype.hide = function () {
        this.display = false;
    };
    return Tabs;
}());
exports.Tabs = Tabs;
//# sourceMappingURL=tabs.js.map