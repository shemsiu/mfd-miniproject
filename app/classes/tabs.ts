export class Tabs {
    title: string;
    slug: string;
    content: string;

    display:boolean;

    constructor(title:string, slug:string, content:string) {
        this.title = title;
        this.slug = slug;
        this.content = content;

        this.hide();
    }

    show() {
        this.display = true;
    }
    hide() {
        this.display = false;
    }
}