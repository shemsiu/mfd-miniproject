import { Component, OnInit } from '@angular/core';
import {Email} from "./classes/email";


@Component({
    selector: 'contact',
    templateUrl: './app/contact.component.html'
})
export class ContactComponent implements OnInit {
    contact:Email = new Email();

    submitted:boolean = false;

    constructor() { }

    ngOnInit() {
    }

    sendmail() {
        this.submitted = true;
    }

}
