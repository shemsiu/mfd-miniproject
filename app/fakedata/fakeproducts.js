"use strict";
var product_1 = require('../classes/product');
var tabs_1 = require("../classes/tabs");
var fisk = "<p class=\"tabs__header\">1. Description:</p>\n        <p class=\"tabs__text\">With its beautiful premium leather, lace-up oxford styling, recycled rubber outsoles and\n            9-inch height, this Earthkeepers\u00AE City Premium style is an undeniably handsome boot. To complement its\n            rustic, commanding outer appearance, we've paid attention to the inside as well - by adding SmartWool\u00AE faux\n            shearling to the linings and crafting the footbed using our exclusive anti-fatigue comfort foam technology\n            to absorb shock. Imported.</p>\n        <p class=\"tabs__subheader\">Detail</p>\n        <ul class=\"tabs__list tabs__list--square\">\n            <li class=\"tabs__list__item tabs__list__item--detail\">Premium burnished full-grain leather and suede upper\n            </li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">Leather is from a tannery rated Silver for its water,\n                energy and waste-management practices\n            </li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">Leather lining and footbed for a premium feel and\n                optimal comfort\n            </li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">SmartWool\u00AE faux shearling lining is made with 60%\n                merino wool and 40% PET\n            </li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">Reflective insole board for additional warmth under\n                foot\n            </li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">100% organic cotton laces</li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">SmartWool\u00AE fabric-lined anti-fatigue footbed provides\n                superior, all-day comfort and climate control\n            </li>\n            <li class=\"tabs__list__item tabs__list__item--detail\">Timberland\u00AE exclusive Gripstick\u2122 and Green Rubber\u2122\n                outsole is made with 42% recycled rubber\n            </li>\n        </ul>";
exports.PRODUCTS = [
    new product_1.Product(1, "Adidas 1", "Impossible is Nothing.", "adidas/logo.jpg", ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg', 'adidas/4.jpg', 'adidas/5.jpg', 'adidas/6.jpg'], 375, 19, "You can fly with these shoes!", 0, ['38', '40', 'String is also an option', '41', '42', '43', '44'], 14, 0 /* adidas */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)"),
        new tabs_1.Tabs("Vigan", "slug-4", "Jep dynamic! :D")
    ]), new product_1.Product(2, "Nike 1", "JUST DO IT!", "nike/logo.png", ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'], 158, 38, "We all love Jordans!", 0, ['42', '43', 'String is also an option', '44'], 2, 1 /* nike */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]), new product_1.Product(3, "Adidas 2", "Impossible is Nothing.", "adidas/logo.jpg", ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg'], 655, 88, "You can fly with these shoes!", 26, ['38', '40', '41', '42', 'String is also an option', '43', '44'], 86, 0 /* adidas */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]),
    new product_1.Product(4, "Nike 2", "JUST DO IT!", "nike/logo.png", ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'], 687, 168, "We all love Jordans!", 0, ['42', '43', 'String is also an option', '44'], 84, 1 /* nike */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]), new product_1.Product(5, "Adidas 3", "Impossible is Nothing.", "adidas/logo.jpg", ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg'], 784, 184, "You can fly with these shoes!", 78, ['38', 'String is also an option', '40', '41', '42', '43', '44'], 14, 0 /* adidas */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]), new product_1.Product(6, "Nike 3", "JUST DO IT!", "nike/logo.png", ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'], 325, 38, "We all love Jordans!", 0, ['42', '43', '44'], 2, 1 /* nike */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]), new product_1.Product(7, "Adidas 4", "Impossible is Nothing.", "adidas/logo.jpg", ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg', 'adidas/4.jpg', 'adidas/5.jpg', 'adidas/6.jpg'], 150, 25, "You can fly with these shoes!", 78, ['38', '40', '41', '42', '43', '44'], 14, 0 /* adidas */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]),
    new product_1.Product(8, "Nike 4", "JUST DO IT!", "nike/logo.png", ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'], 325, 38, "We all love Jordans!", 0, ['42', '43', '44'], 2, 1 /* nike */, [
        new tabs_1.Tabs("Product overview", "slug-1", fisk),
        new tabs_1.Tabs("Features", "slug-2", "Some content her ..."),
        new tabs_1.Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
    ]),
];
//# sourceMappingURL=fakeproducts.js.map