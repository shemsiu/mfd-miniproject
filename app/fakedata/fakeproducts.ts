import {Product} from '../classes/product';
import {Brand} from "../classes/brand";
import {Tabs} from "../classes/tabs";

let fisk = `<p class="tabs__header">1. Description:</p>
        <p class="tabs__text">With its beautiful premium leather, lace-up oxford styling, recycled rubber outsoles and
            9-inch height, this Earthkeepers® City Premium style is an undeniably handsome boot. To complement its
            rustic, commanding outer appearance, we've paid attention to the inside as well - by adding SmartWool® faux
            shearling to the linings and crafting the footbed using our exclusive anti-fatigue comfort foam technology
            to absorb shock. Imported.</p>
        <p class="tabs__subheader">Detail</p>
        <ul class="tabs__list tabs__list--square">
            <li class="tabs__list__item tabs__list__item--detail">Premium burnished full-grain leather and suede upper
            </li>
            <li class="tabs__list__item tabs__list__item--detail">Leather is from a tannery rated Silver for its water,
                energy and waste-management practices
            </li>
            <li class="tabs__list__item tabs__list__item--detail">Leather lining and footbed for a premium feel and
                optimal comfort
            </li>
            <li class="tabs__list__item tabs__list__item--detail">SmartWool® faux shearling lining is made with 60%
                merino wool and 40% PET
            </li>
            <li class="tabs__list__item tabs__list__item--detail">Reflective insole board for additional warmth under
                foot
            </li>
            <li class="tabs__list__item tabs__list__item--detail">100% organic cotton laces</li>
            <li class="tabs__list__item tabs__list__item--detail">SmartWool® fabric-lined anti-fatigue footbed provides
                superior, all-day comfort and climate control
            </li>
            <li class="tabs__list__item tabs__list__item--detail">Timberland® exclusive Gripstick™ and Green Rubber™
                outsole is made with 42% recycled rubber
            </li>
        </ul>`;

export const PRODUCTS: Product[] = [
    new Product(
        1,
        "Adidas 1",
        "Impossible is Nothing.",
        "adidas/logo.jpg",
        ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg', 'adidas/4.jpg', 'adidas/5.jpg', 'adidas/6.jpg'],
        375,
        19,
        "You can fly with these shoes!",
        0,
        ['38', '40', 'String is also an option', '41', '42', '43', '44'],
        14,
        Brand.adidas,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)"),
            new Tabs("Vigan", "slug-4", "Jep dynamic! :D")
        ]
    ), new Product(
        2,
        "Nike 1",
        "JUST DO IT!",
        "nike/logo.png",
        ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'],
        158,
        38,
        "We all love Jordans!",
        0,
        ['42', '43', 'String is also an option', '44'],
        2,
        Brand.nike,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ), new Product(
        3,
        "Adidas 2",
        "Impossible is Nothing.",
        "adidas/logo.jpg",
        ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg'],
        655,
        88,
        "You can fly with these shoes!",
        26,
        ['38', '40', '41', '42', 'String is also an option', '43', '44'],
        86,
        Brand.adidas,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ),
    new Product(
        4,
        "Nike 2",
        "JUST DO IT!",
        "nike/logo.png",
        ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'],
        687,
        168,
        "We all love Jordans!",
        0,
        ['42', '43','String is also an option', '44'],
        84,
        Brand.nike,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ),    new Product(
        5,
        "Adidas 3",
        "Impossible is Nothing.",
        "adidas/logo.jpg",
        ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg'],
        784,
        184,
        "You can fly with these shoes!",
        78,
        ['38', 'String is also an option', '40', '41', '42', '43', '44'],
        14,
        Brand.adidas,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ), new Product(
        6,
        "Nike 3",
        "JUST DO IT!",
        "nike/logo.png",
        ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'],
        111,
        11,
        "We all love Jordans!",
        0,
        ['42', '43', 'String is also an option', '44'],
        11,
        Brand.nike,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ), new Product(
        7,
        "Adidas 4",
        "Impossible is Nothing.",
        "adidas/logo.jpg",
        ['adidas/1.jpg', 'adidas/2.jpg', 'adidas/3.jpg', 'adidas/4.jpg', 'adidas/5.jpg', 'adidas/6.jpg'],
        150,
        25,
        "You can fly with these shoes!",
        78,
        ['38', '40', '41', '42', '43', '44'],
        14,
        Brand.adidas,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ),
    new Product(
        8,
        "Nike 4",
        "JUST DO IT!",
        "nike/logo.png",
        ['nike/1.jpg', 'nike/2.jpg', 'nike/3.jpg', 'nike/4.jpg', 'nike/5.jpg', 'nike/6.jpg'],
        325,
        38,
        "We all love Jordans!",
        0,
        ['42', '43', '44'],
        2,
        Brand.nike,
        [
            new Tabs("Product overview", "slug-1", fisk),
            new Tabs("Features", "slug-2", "Some content her ..."),
            new Tabs("Customer Review", "slug-3", "Hey? This is dynamic content! :)")
        ]
    ),
];