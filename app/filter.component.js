"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var FilterComponent = (function () {
    function FilterComponent(elementRef) {
        this.elementRef = elementRef;
        this.colors = ['#e82026', '#f9a058', '#58a8f9', '#dbe47b', '#610b0e', '#781c70',
            '#1b334c', '#517737', '#7520e8', '#4c3d31', '#060606', '#1a6a0b', '#a0629f', '#074a74', '#beadad',
            '#f65812', '#c4da0f', '#377377', '#bd8b7a'];
    }
    FilterComponent.prototype.ngOnInit = function () {
    };
    FilterComponent.prototype.ngAfterViewInit = function () {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/js/classes/rangeslider.js";
        this.elementRef.nativeElement.appendChild(s);
    };
    FilterComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'filter',
            templateUrl: './filter.component.html'
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], FilterComponent);
    return FilterComponent;
}());
exports.FilterComponent = FilterComponent;
//# sourceMappingURL=filter.component.js.map