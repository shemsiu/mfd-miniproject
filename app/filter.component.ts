import {Component, OnInit, ElementRef} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'filter',
    templateUrl: './filter.component.html'
})
export class FilterComponent implements OnInit {
    colors = ['#e82026', '#f9a058', '#58a8f9', '#dbe47b', '#610b0e', '#781c70',
        '#1b334c', '#517737', '#7520e8', '#4c3d31', '#060606', '#1a6a0b', '#a0629f', '#074a74', '#beadad',
        '#f65812', '#c4da0f', '#377377', '#bd8b7a'];

    constructor(private elementRef:ElementRef) {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/js/classes/rangeslider.js";
        this.elementRef.nativeElement.appendChild(s);
    }
}
