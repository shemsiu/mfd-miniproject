import { Component, OnInit } from '@angular/core';

import {HeaderSliderComponent} from "./headerslider.component";
import {FeaturedComponent} from "./featured.component";
import {SpecialProductsComponent} from "./specialproduct.component";

@Component({
    selector: 'frontpage',
    directives: [HeaderSliderComponent, FeaturedComponent, SpecialProductsComponent],
    templateUrl: './app/frontpage.component.html'
})
export class FrontpageComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

}