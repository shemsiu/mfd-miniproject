import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';


@Component({
    selector: 'my-header',
    templateUrl: './app/header.component.html'
})
export class HeaderComponent implements OnInit {

    title: string = "Velkommen til min side!";

    constructor(private router: Router) {

    }
    ngOnInit() {

    }

    goFrontpage() {
        this.router.navigateByUrl('/');
    }

}