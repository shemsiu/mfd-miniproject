import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
    selector: 'headerslider',
    templateUrl: './app/headerslider.component.html'
})
export class HeaderSliderComponent implements OnInit {
    constructor(private elementRef:ElementRef) { }

    ngOnInit() { }

    ngAfterViewInit() {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/js/classes/slider.js";
        this.elementRef.nativeElement.appendChild(s);
    }
}