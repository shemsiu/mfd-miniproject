"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var product_1 = require("./classes/product");
var ProductDetailComponent = (function () {
    function ProductDetailComponent(elementRef) {
        this.elementRef = elementRef;
    }
    ProductDetailComponent.prototype.ngOnInit = function () {
    };
    ProductDetailComponent.prototype.ngAfterViewInit = function () {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/js/classes/tabs.js";
        var a = document.createElement("script");
        a.type = "text/javascript";
        a.src = "./assets/js/classes/thumbslider.js";
        this.elementRef.nativeElement.appendChild(s);
        this.elementRef.nativeElement.appendChild(a);
    };
    ProductDetailComponent.prototype.addToCart = function (product, qty) {
        product.minusStock(qty);
        alert("You just bought " + qty + " pair of " + product.title);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', product_1.Product)
    ], ProductDetailComponent.prototype, "product", void 0);
    ProductDetailComponent = __decorate([
        core_1.Component({
            selector: 'productdetail',
            templateUrl: './app/productdetail.component.html'
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef])
    ], ProductDetailComponent);
    return ProductDetailComponent;
}());
exports.ProductDetailComponent = ProductDetailComponent;
//# sourceMappingURL=productdetail.component.js.map