import {Component, OnInit, Input, ElementRef} from '@angular/core';

import {Product} from "./classes/product";


@Component({
    selector: 'productdetail',
    templateUrl: './app/productdetail.component.html'
})
export class ProductDetailComponent implements OnInit {
    @Input() product: Product;

    constructor(private elementRef:ElementRef) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        let s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "./assets/js/classes/tabs.js";
        let a = document.createElement("script");
        a.type = "text/javascript";
        a.src = "./assets/js/classes/thumbslider.js";

        this.elementRef.nativeElement.appendChild(s);
        this.elementRef.nativeElement.appendChild(a);
    }

    addToCart(product:Product, qty:number) {
        product.minusStock(qty);
        alert("You just bought " + qty + " pair of " + product.title)
    }
}