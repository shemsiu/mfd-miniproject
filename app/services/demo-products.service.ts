import { Injectable } from '@angular/core';
import { PRODUCTS } from '../fakedata/fakeproducts';

@Injectable()
export class DemoProductService {

    constructor() { }

    getDemoData() {
        return PRODUCTS;
    }

    getDemoDataById(id:number) {
        let result = PRODUCTS.filter(function(obj) {
            return obj.id == id;
        });
        if(result.length > 0) {
            return result[0];
        } else {
            return null;
        }
    }
}