"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var demo_products_service_1 = require("./services/demo-products.service");
var filter_component_1 = require("./filter.component");
var productdetail_component_1 = require("./productdetail.component");
var SingleProductComponent = (function () {
    function SingleProductComponent(demoData, route) {
        this.demoData = demoData;
        this.route = route;
    }
    SingleProductComponent.prototype.ngOnInit = function () {
        window.scrollTo(0, 0);
        var productId = this.route.snapshot.params['productId'];
        this.product = this.demoData.getDemoDataById(productId);
        this.childProduct = this.product;
    };
    SingleProductComponent = __decorate([
        core_1.Component({
            selector: 'singleproduct',
            templateUrl: './app/singleproduct.component.html',
            directives: [filter_component_1.FilterComponent, productdetail_component_1.ProductDetailComponent],
            providers: [demo_products_service_1.DemoProductService]
        }), 
        __metadata('design:paramtypes', [demo_products_service_1.DemoProductService, router_1.ActivatedRoute])
    ], SingleProductComponent);
    return SingleProductComponent;
}());
exports.SingleProductComponent = SingleProductComponent;
//# sourceMappingURL=singleproduct.component.js.map