import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params} from '@angular/router';
import {DemoProductService} from "./services/demo-products.service";
import {Product} from "./classes/product";
import {FilterComponent} from "./filter.component";
import {ProductDetailComponent} from "./productdetail.component";

@Component({
    selector: 'singleproduct',
    templateUrl: './app/singleproduct.component.html',
    directives: [FilterComponent, ProductDetailComponent],
    providers: [DemoProductService]
})
export class SingleProductComponent implements OnInit {
    product: Product;
    childProduct:Product;

    constructor(private demoData: DemoProductService, private route:ActivatedRoute) { }

    ngOnInit() {
        window.scrollTo(0,0);

        let productId = this.route.snapshot.params['productId'];
        this.product = this.demoData.getDemoDataById(productId);
        this.childProduct = this.product;
    }
}