import { Component, OnInit } from '@angular/core';
import {DemoProductService} from "./services/demo-products.service";
import {Product} from "./classes/product";

@Component({
    selector: 'specialproducts',
    templateUrl: './app/specialproducts.component.html',
    providers: [DemoProductService]
})
export class SpecialProductsComponent implements OnInit {
    products: Product[];

    constructor(private demoData:DemoProductService) { }

    ngOnInit() {
        this.products = this.demoData.getDemoData();
    }
}