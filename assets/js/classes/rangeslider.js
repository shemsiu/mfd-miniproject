function data ( element, key ) {
    return element.getAttribute('data-' + key);
}

function createSlider ( slider ) {

    noUiSlider.create(slider, {
        start: [45, 190],
        connect: false,
        margin: 50,
        step: Number(data(slider, 'step')) || 5,
        range: {
            'min': 0,
            'max': 250
        },
        tooltips: true,
        connect: true,
        format: {
            to: function (value) {
                return value + '$';
            },
            from: function (value) {
                return value.replace('', '');
            }
        }
    });
}

Array.prototype.forEach.call(document.querySelectorAll('.slider'), createSlider);