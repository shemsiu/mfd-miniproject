var slideIndex = 1;
showDivs(slideIndex);
function plusDivs(n) {
    showDivs(slideIndex += n);
}
function showDivs(n) {
    var x = $('.slideshow__content');

    if (n > x.length)
        slideIndex = 1;

    if (n < 1)
        slideIndex = x.length;

    for (var i = 0; i < x.length; i++)
        x[i].style.display = "none";


    x[slideIndex - 1].style.display = "block";

    $(".slideshow__image").css('background-image', 'url(./assets/img/' + x[slideIndex - 1].getAttribute("data-img") + ')');
}