//TAB
openTab("slug-1");

$(".tabslistitem").on('click', function(e) {
    openTab(this.className.split(' ')[0]);
});

function openTab(tab) {
    var x = $('.data-tab');

    for (var i = 0; i < x.length; i++) {
        x[i].style.display = "none";
        $(".tabs__list__item").removeClass("tabs__list__item--active");
    }

    $("#" + tab).css('display', 'block');
    $("." + tab).addClass("tabs__list__item--active");
}