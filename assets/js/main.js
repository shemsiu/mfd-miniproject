var colors = ['e82026', 'f9a058', '58a8f9', 'dbe47b', '610b0e', '781c70',
    '1b334c', '517737', '7520e8', '4c3d31', '060606', '1a6a0b', 'a0629f', '074a74', 'beadad',
    'f65812', 'c4da0f', '377377', 'bd8b7a'];
$(document).ready(function () {
    colors.forEach(function (t) {
        $("#colorselector__list").append('<div class="filter__color__single" style="background-color:#' + t + '"></div>');
    });
});
