var gulp = require('gulp'),
    sass = require('gulp-sass'),
    util = require('gulp-util'),
    minifycss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer');


gulp.task('serve', function () {
    gulp.watch("source/scss/**/*.scss", ['build-css']);
});

gulp.task('build-css', function () {
    return gulp.src('source/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        //.on('error', onError)
        .pipe(minifycss({keepSpecialComments : 0}))
        .pipe(postcss([autoprefixer({browsers: ['last 2 versions']})]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets/css'));
});

gulp.task('watch', function () {
    gulp.watch('source/scss/**/*.scss', ['build-css']);
});

gulp.task('default', ['watch', 'serve']);